export interface Cohorte {
    fecha_inicio?: string,
    fecha_fin?: string,
    habilitado?: boolean,
}