/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { MatDialog } from '@angular/material/dialog';
import { RecoveryPassComponent } from '../recovery-pass/recovery-pass.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import swal from 'sweetalert2';
import { AuthService } from '../../services/auth.service';
import { JwtService } from '../../services/jwt.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent {

  public mainForm: FormGroup;
  public dissabledLogin = false;

  constructor(
    private _router: Router,
    public dialog: MatDialog,
    private _authService: AuthService,
    private _fb: FormBuilder,
    private jwlHelper: JwtService,
    private spinner: NgxSpinnerService
  ) {
    this.mainForm = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  public userLogin() {
    const email = this.mainForm.get('email')?.value;
    const password = this.mainForm.get('password')?.value;
    this.spinner.show();
    this.dissabledLogin = true; //con esta variable bloqueamos los botones de crear cuenta y recuperar contraseña cuando se marque el iniciar sesion.

    this._authService.login(email, password).subscribe({
      next: ( res: any) => {
        const role = this.jwlHelper.getRole();
        this.spinner.hide();
        this._router.navigateByUrl(this.getNavigateUrl(role))
      },
      error: (err: unknown) => {
        this.spinner.hide();
        this.dissabledLogin = false;
        swal.fire('¡Error!', 'Este correo no se encuentra registrado', 'error');
      },
    });

  }

  public openSignUpDialog(): void {
    const dialogRef = this.dialog.open(SignUpComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }
  public openRecoverPassDialog(): void {
    const dialogRef = this.dialog.open(RecoveryPassComponent);

    dialogRef.afterClosed().subscribe((result) => {});
  }

  /**
   * Método para retornar la url de navegacion dado el rol.
   * @param role role del usuario
   * @returns la url a navegar
   */
  public getNavigateUrl(role: string) {
    const urls: { [key: string]: string } = {
      USUARIO: '/inscription/aspirante/personal-info',
      ADMIN: '/inscription/backlog/home',
      ENCARGADO: '/inscription/backlog/home',
    };

    return urls[role];
  }
}
