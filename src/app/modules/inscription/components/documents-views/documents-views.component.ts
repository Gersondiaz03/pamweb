import { Component, Input, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';
import { JwtService } from 'src/app/modules/auth/services/jwt.service';
import { DocsService } from '../../services/docs.service';
import { tap } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { saveAs } from 'file-saver';


@Component({
  selector: 'app-documents-views',
  templateUrl: './documents-views.component.html',
  styleUrls: ['./documents-views.component.css'],
})
export class DocumentsViewsComponent implements OnInit {
  public isConfirmed = false;

  public docs!: any;

  public rejectDoc!: any;

  public selectReject = '';

  public other = '';

  public showModalReject = false;

  @Input() user!: any;

  constructor(
    private http: HttpClient,
    private jwt: JwtService,
    private docService: DocsService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this.jwt.getToken(),
      }),
    };
    this.http.get(`http://localhost:8080/doc/listarDoc?aspiranteId=${this.user.id}`,
        httpOptions) .subscribe((docs) => (this.docs = docs));
  }

  public confirmTrigger(document: string, idDoc: number, index: number) {
    Swal.fire({
      title: `¿Estás seguro de que deseas aprobar el documento ${document} ?`,
      text: 'Esta acción no se puede deshacer',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, confirmar',
    }).then((result) => {
      if (result.isDenied || result.dismiss) {
        return;
      }

      // hago el llamado a la api
      this.docService
        .acceptDoc({ idDoc, idAspirant: this.user.id })
        .pipe(
          tap((doc) => {
            //TODO: esto se tiene qeu cambiar, no es lo ideal. Se debe de implementar bloquear los botones.
            this.docs[index].estado.id = 4;
            this.docs[index].estado.nombre = 'aprobado';
          })
        )
        .subscribe();
    });
  }

  public getColorTarget(estado: number) {
    const classNg: { [key: number]: string } = {
      2: 'bg-gray-200',
      3: 'bg-red-500 text-white',
      4: 'bg-green-500 text-white',
    };
    return classNg[estado];
  }

  public rejectDocument(option: number) {
    if (option == -1) {
      this.showModalReject = false;
      return;
    }

    const data = {
      docId: this.rejectDoc.documento.id,
      aspiranteId: this.user.id,
      retroalimentacion: this.selectReject === 'Otro' ? this.other : this.selectReject,
    };
    this.showModalReject = false;
    this.docService
      .rejectDoc(data)
      .pipe(
        tap((doc) => {
          //TODO: Tratar de mejorar la actualizacion en tiempo real para el rechazo de doucumentos.
          const index = this.docs.findIndex(
            (doc: any) =>
              doc.documento.id === this.rejectDoc.documento.id
          );
          this.docs[index].estado.id = 3;
          this.docs[index].estado.nombre = 'Rechazado';
        })
      )
      .subscribe();
  }

  public reject(doc: any) {
    this.showModalReject = true;
    this.rejectDoc = doc;
  }

  public downloadAllDocuments(){
    // this.spinner.hide();
    this.docService.downloadDocuments(this.user.id).subscribe({
      next: res => {
        saveAs(res, `${this.user.id}. ${this.user.nombre + ' '+ this.user.apellido}.zip`);
      },
      error: err => {
        console.log(err)
      }
    })
  }
}
