/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { S3Service } from '../../services/s3.service';
import { DocumentsService } from '../../services/documents.service';
import { Subscription } from 'rxjs';
import { JwtService } from 'src/app/modules/auth/services/jwt.service';
import { ApplicantService } from 'src/app/modules/auth/services/applicant.service';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css'],
})
export class DocumentsComponent implements OnInit {
  mainForm: FormGroup;
  counter = 0;
  unableToUploadDocuments = false;
  isForeign = '';
  isEgresado = false;
  docs: [] = [];
  estadoDocs: string[] = [];

  photos = File;
  photoStatus = '';
  photoStatusId = 0;
  photosFileName = '';

  passport = '';
  passportStatus = '';
  passportFileName = '';

  undergraduateDiploma = '';
  undergraduateDiplomaStatus = '';
  undergraduateDiplomaName = '';

  undergraduateNotes = '';
  undergraduateNotesStatus = '';
  underGraduateNotesName = '';

  commitmentAct = '';
  commitmentActStatus = '';
  commitmentActName = '';

  applicantCV = '';
  applicantCVStatus = '';
  applicantCVName = '';

  inscriptionRights = '';
  inscriptionRightsStatus = '';
  inscriptionRightsName = '';

  digitalSign = '';
  digitalSignStatus = '';
  digitalSignName = '';

  academicReferenceOne = '';
  academicReferenceOneStatus = '';
  academicReferenceOneName = '';

  academicReferenceTwo = '';
  academicReferenceTwoStatus = '';
  academicReferenceTwoName = '';

  inscriptionFormat = '';
  inscriptionFormatStatus = '';
  inscriptionFormatName = '';

  changeSub: Subscription;

  constructor(
    private _fb: FormBuilder,
    private _s3: S3Service,
    private _documentsService: DocumentsService,
    private _jwt: JwtService,
    private _applicant: ApplicantService,
    private _router: Router,
    private _spinner: NgxSpinnerService
  ) {
    this.changeSub = new Subscription();

    this.mainForm = this._fb.group({
      photos: ['', Validators.required],
      passport: ['', Validators.required],
      undergraduateDiploma: ['', Validators.required],
      undergraduateNotes: ['', Validators.required],
      commitmentAct: ['', Validators.required],
      applicantCV: ['', Validators.required],
      inscriptionRights: ['', Validators.required],
      sign: ['', Validators.required],
      academicReferenceOne: ['', Validators.required],
      academicReferenceTwo: ['', Validators.required],
      inscriptionFormat: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    const userId = this._jwt.unDecodeAllToken().idUser;

    this._applicant.getApplicant(userId).subscribe({
      next: (response) => {
        this.isForeign = response.lugar_nac;
        this.isEgresado = response.es_egresado;
      },
      error: (error) => {
        console.error('Error:', error);
      },
    });

    /**
     * Método para actualizar el estado del documento enviado
     *
     * @params El idDocumento que trae el endpoint
     * @returns El estado del documento correspondiente actualizado
     */
    this._documentsService.listUserDocuments(userId).subscribe({
      next: (response: any[]) => {
        for (const docs of response) {
          if (docs.idDocumento == 1) {
            this.photoStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 2) {
            this.passportStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 3) {
            this.undergraduateDiplomaStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 4) {
            this.undergraduateNotesStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 5) {
            this.commitmentActStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 6) {
            this.applicantCVStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 7) {
            this.inscriptionRightsStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 8) {
            this.digitalSignStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 9) {
            this.academicReferenceOneStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 10) {
            this.academicReferenceTwoStatus = docs.estado.nombre;
          }
          if (docs.idDocumento == 11) {
            this.inscriptionFormatStatus = docs.estado.nombre;
          }
          // console.log(docs.idDocumento + docs.estado.nombre);
        }
        response.forEach((doc) => {
          this.estadoDocs.push(doc.estado.nombre);
        });
      },
      error: (error) => {
        console.error('Error:', error);
      },
    });
  }

  onPhotosSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.photosFileName = file![0].name;
    this.mainForm.get('photos')?.setValue(file![0]);
    swal.fire('¡Felicidades!', '¡changed!', 'success');
  }

  onPhotosUpload() {
    const file = this.mainForm.get('photos')?.value;
    const fileType = file.type;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    if (fileType !== 'image/jpeg' && fileType !== 'image/png') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo JPG o PNG!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(1, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tus fotos con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onPassportSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.passportFileName = file![0].name;
    this.mainForm.get('passport')?.setValue(file![0]);
  }

  onPassportUpload() {
    const file = this.mainForm.get('passport')?.value;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(2, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tu pasaporte con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onUndergraduateDiplomaSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.undergraduateDiplomaName = file![0].name;
    this.mainForm.get('undergraduateDiploma')?.setValue(file![0]);
  }

  onUnderGraduateDiplomaUpload() {
    const file = this.mainForm.get('undergraduateDiploma')?.value;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;

    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(3, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tu diploma de pregrado con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onUndergraduateNotesSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.underGraduateNotesName = file![0].name;
    this.mainForm.get('undergraduateNotes')?.setValue(file![0]);
  }

  onUnderGraduateNotesUpload() {
    const file = this.mainForm.get('undergraduateNotes')?.value;
    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(4, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tus notas de pregrado con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onCommitmentActSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.commitmentActName = file![0].name;
    this.mainForm.get('commitmentAct')?.setValue(file![0]);
  }

  onCommitmentActUpload() {
    const file = this.mainForm.get('commitmentAct')?.value;
    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(5, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tu acta de compromiso con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onApplicantCVSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.applicantCVName = file![0].name;
    this.mainForm.get('applicantCV')?.setValue(file![0]);
  }

  onApplicantCVUpload() {
    const file = this.mainForm.get('applicantCV')?.value;
    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(6, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tu hoja de vida con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onInscriptionRightsSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.inscriptionRightsName = file![0].name;
    this.mainForm.get('inscriptionRights')?.setValue(file![0]);
  }

  onInscriptionRightsUpload() {
    const file = this.mainForm.get('inscriptionRights')?.value;
    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(7, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tus derechos de inscripción con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onSignSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.digitalSignName = file![0].name;
    this.mainForm.get('sign')?.setValue(file![0]);
  }

  onSignUpload() {
    const file = this.mainForm.get('sign')?.value;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }

    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(8, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido tu firma digital con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onAcademicReferenceOneSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.academicReferenceOneName = file![0].name;
    this.mainForm.get('academicReferenceOne')?.setValue(file![0]);
  }

  onAcademicReferenceOneUpload() {
    const file = this.mainForm.get('academicReferenceOne')?.value;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }
    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(9, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido la carta de referencia académica #1 con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onAcademicReferenceTwoSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.academicReferenceTwoName = file![0].name;
    this.mainForm.get('academicReferenceTwo')?.setValue(file![0]);
  }

  onAcademicReferenceTwoUpload() {
    const file = this.mainForm.get('academicReferenceTwo')?.value;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }
    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(10, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido la carta de referencia académica #2 con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  onInscriptionFormatSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.inscriptionFormatName = file![0].name;
    this.mainForm.get('inscriptionFormat')?.setValue(file![0]);
  }

  onInscriptionFormatUpload() {
    const file = this.mainForm.get('inscriptionFormat')?.value;

    if (!file) {
      swal.fire(
        '¡Error!',
        '¡No has subido ningún archivo, seleccionalo!',
        'error'
      );
      return;
    }
    const fileName = file.name;
    const fileExtension = fileName.split('.').pop()?.toLowerCase();

    if (fileExtension !== 'pdf') {
      swal.fire(
        '¡Error!',
        '¡El archivo que has subido no es de tipo PDF!',
        'error'
      );
      return;
    } else {
      this._spinner.show();
      this._s3.uploadFile(11, file).subscribe((response) => {
        console.log(response);
        swal.fire(
          '¡Felicidades!',
          '¡Has subido el formato de inscripción con éxito!',
          'success'
        );
        const currentUrl = this._router.url;
        const navigationExtras: NavigationExtras = {
          skipLocationChange: true,
        };
        this._router.navigateByUrl('/', navigationExtras).then(() => {
          this._router.navigate([currentUrl], navigationExtras);
        });
        this._spinner.hide();
      });
    }
  }

  reloadCurrentRoute() {
    this._router.navigate([this._router.url]);
  }

  getBadgeClass(status: string) {
    if (status === 'APROBADO') {
      return 'badge-approved';
    } else if (status === 'RECHAZADO') {
      return 'badge-rejected';
    } else if (status === 'SIN REVISAR') {
      return 'badge-revission';
    } else {
      return;
    }
  }

  getDocsStatus(): boolean {
    const validStatus = ['APROBADO', 'SIN REVISAR'];
    const allValid = this.estadoDocs.every((estado) =>
      validStatus.includes(estado)
    );
    const allMatched = this.estadoDocs.length === 11 && allValid;
    return allMatched;
  }

  redirectToCommitmentActFormat(): void {
    const url =
      'https://educaiton.cloud.ufps.edu.co/rsc/formatos/inscripcion/actacompromisonotas.docx';
    window.open(url, '_blank');
  }

  redirectToCVFormat(): void {
    const url =
      'https://educaiton.cloud.ufps.edu.co/rsc/formatos/inscripcion/formatohojadevida.docx';
    window.open(url, '_blank');
  }

  redirectToAcademicReferenceFormat(): void {
    const url =
      'https://educaiton.cloud.ufps.edu.co/rsc/formatos/inscripcion/carta_modelo_de_referencia.docx';
    window.open(url, '_blank');
  }

  redirectToInscriptionFormat(): void {
    const url =
      'https://educaiton.cloud.ufps.edu.co/rsc/formatos/inscripcion/formatoinscripcion.docx';
    window.open(url, '_blank');
  }
}
