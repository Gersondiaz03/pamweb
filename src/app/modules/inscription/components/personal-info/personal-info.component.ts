/* eslint-disable @typescript-eslint/no-inferrable-types */
import {
  ChangeDetectorRef,
  Component,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CountriesService } from '../../services/countries.service';
import { Country } from '../../interfaces/country';
import { StatesService } from '../../services/states.service';
import { ColombianStatesAndCities } from '../../interfaces/colombian-states';
import { ApplicantService } from 'src/app/modules/auth/services/applicant.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { StepperComponent } from '../stepper/stepper.component';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PersonalInfoComponent {
  public value: string = '';
  public selectedIndex: number = 0;
  public maxNumberOfTabs: number = 3;
  public mainForm: FormGroup;
  public maxDate: Date;

  public countries: Country[] = [];
  public selectedCountry: Country = {} as Country;
  public states: ColombianStatesAndCities[] = [];
  public towns!: any;
  public foreignCountry: boolean = false;
  public isForeign: boolean = false;
  public isDropdownFocused = false;

  public genders: any[] = [
    { name: 'Masculino', key: 'M' },
    { name: 'Femenino', key: 'F' },
    { name: 'Otro', key: 'X' },
  ];

  public tabs = [true, false, false];

  constructor(
    private _fb: FormBuilder,
    private _ctr: CountriesService,
    private _clmbnSts: StatesService,
    private _applicant: ApplicantService,
    private _datePipe: DatePipe,
    private spinner: NgxSpinnerService,
    private auth: AuthService
  ) {
    this.maxDate = new Date();

    this.mainForm = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      familyName: ['', Validators.required],
      id: ['',[Validators.required, Validators.pattern(/^[0-9]*$/)]],
      idExpeditionDate: ['', Validators.required],
      birthDate: ['', Validators.required],
      countryOfBirth: ['Colombia', Validators.required],
      gender: ['', Validators.required],
      residenceState: [''],
      residenceTown: [''],
      residenceDirection: ['', Validators.required],
      telephoneNumber: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^\d{10}$/)]],

      workingEntity: ['', Validators.required],
      workingState: ['', Validators.required],
      workingTown: ['', Validators.required],
      workingDirection: ['', Validators.required],

      undergraduateStudies: ['', Validators.required],
      graduatedFromUFPS: ['', Validators.required],
      postgraduateStudies: ['', Validators.required],
      workingExperience: ['', Validators.required],
    });

    this._ctr.getCountry().subscribe((countries) => {
      this.countries = countries;
    });

    this._clmbnSts.getState().subscribe((states) => {
      this.states = states
        .map((state) => ({
          id: 0,
          departamento: state.departamento,
          ciudades: [state.municipio],
        }))
        .filter(
          (value, index, self) =>
            self.findIndex((s) => s.departamento === value.departamento) ===
            index
        );
    });
  }

  public setMunicipio(selectedState: string) {
    this._clmbnSts.getTowns().subscribe((states) => {
      const matchingState = states.find(
        (state) => state.departamento === selectedState
      );
      if (matchingState) {
        this.towns = matchingState.ciudades.map((city) => ({ name: city }));
      }
    });
  }

  public onForeignCountryChange(selectedCountry: string) {
    this.foreignCountry = selectedCountry !== 'Colombia';
    this.mainForm.get('residenceState')?.setValue('Extranjero');
    this.mainForm.get('residenceTown')?.setValue('Extranjero');
    this.isForeign = true;
  }

  public registerApplicant() {
    const email = this.mainForm.get('email')?.value;
    const name = this.mainForm.get('name')?.value;
    const familyName = this.mainForm.get('familyName')?.value;
    const id = this.mainForm.get('id')?.value;
    const idExpeditionDate = this._datePipe.transform(
      this.mainForm.get('idExpeditionDate')?.value,
      'YYYY-MM-dd'
    ) as unknown as Date;
    const birthDate = this._datePipe.transform(
      this.mainForm.get('birthDate')?.value,
      'YYYY-MM-dd'
    ) as unknown as Date;
    const countryOfBirth = this.mainForm.get('countryOfBirth')?.value;
    const gender = this.mainForm.get('gender')?.value.name;
    const residenceState = this.mainForm.get('residenceState')?.value;
    const residenceTown = this.mainForm.get('residenceTown')?.value;
    const residenceDirection = this.mainForm.get('residenceDirection')?.value;
    const telephoneNumber = this.mainForm.get('telephoneNumber')?.value;
    const workingEntity = this.mainForm.get('workingEntity')?.value;
    const workingState = this.mainForm.get('workingState')?.value;
    const workingTown = this.mainForm.get('workingTown')?.value;
    const workingDirection = this.mainForm.get('workingDirection')?.value;
    const undergraduateStudies = this.mainForm.get(
      'undergraduateStudies'
    )?.value;
    const graduatedFromUFPS =
      this.mainForm.get('graduatedFromUFPS')?.value === 'yes' ? true : false;
    const postgraduateStudies = this.mainForm.get('postgraduateStudies')?.value;
    const workingExperience = this.mainForm.get('workingExperience')?.value;

    this.spinner.show();

    this._applicant
      .registerApplicant(
        name,
        familyName,
        gender,
        countryOfBirth,
        idExpeditionDate,
        birthDate,
        id,
        email,
        residenceState,
        workingState,
        residenceTown.name,
        residenceDirection,
        telephoneNumber,
        workingEntity,
        workingTown,
        workingDirection,
        undergraduateStudies,
        postgraduateStudies,
        workingExperience,
        graduatedFromUFPS
      )
      .subscribe({
        next: (res) => {
          this.spinner.hide();
          this.auth.changeStateCurrentUser();
        },
        error: (err) => {
          this.spinner.hide();
          swal.fire(
            '¡Error!',
            'No has diligenciado todos los campos correctamente',
            'error'
          );
        },
      });
  }

  public changeTap(index: number) {
    this.tabs = this.tabs.map((tab, indexTab) => indexTab == index);
  }

  public getControl(control: string) {
    return this.mainForm.get(control);
  }
}

// Basura
/**
  public nextStep() {
    if (this.selectedIndex != this.maxNumberOfTabs) {
      this.selectedIndex = this.selectedIndex + 1;
    }
  }

  public previousStep() {
    if (this.selectedIndex != 0 || this.selectedIndex == this.maxNumberOfTabs) {
      this.selectedIndex = this.selectedIndex - 1;
    }
  }

  public onDropdownFocus() {
    this.isDropdownFocused = true;
    // Manually trigger a change detection cycle to update the view
    this.cdr.detectChanges();
  }

  public onDropdownBlur() {
    this.isDropdownFocused = false;
    // Manually trigger a change detection cycle to update the view
    this.cdr.detectChanges();
  }
 *
 */
