import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
})
export class StepperComponent implements OnInit {

  public selectedStepIndex!: number;
  public steps  = [
    'personal-info',
    'documents',
    'status',
  ];

  public pasos: MenuItem[] = [
    {
      label: 'Datos personales',
      routerLink: 'personal-info',
    },
    {
      label: 'Documentos',
      routerLink: 'documents',
    },
    {
      label: 'Entrevista y prueba',
      routerLink: 'status',
    },
  ];

  constructor(
    private readonly router: Router, 
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.auth.currentUser.subscribe({
      next: (user) => {
        if(user != null){
          this.selectedStepIndex =  this.validateState(user?.status);
          this.router.navigate([`/inscription/aspirante/${this.steps[this.selectedStepIndex]}`]);
        }
      },
    });
  }

  /**
   * Método para validar el estado del aspirante y controlar el stepper
   * @param status estado actual del aspirante
   * @returns el indece para navegación en el stepper
   */
  private validateState( status: number ){
    if(status >= 2 && status <= 4)
      return 1
    
    if(status >= 5)
      return 2
    
    return 0
  }
}
