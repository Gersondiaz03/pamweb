import { CommonModule, DatePipe } from "@angular/common";
import { AngularMaterialModule } from "../angular-material/angular-material.module";
import { StepperComponent } from "./components/stepper/stepper.component";
import { PersonalInfoComponent } from "./components/personal-info/personal-info.component";
import { NgModule } from "@angular/core";
import { InscriptionRoutingModule } from "./inscription.routing.module";
import { RouterModule } from '@angular/router';
import { DocumentsComponent } from "./components/documents/documents.component";
import { StatusComponent } from "./components/status/status.component";
import { TableComponent } from './pages/home/table.component';
import { HttpClientModule } from '@angular/common/http';
import { DocsReviewComponent } from './pages/docs-review/docs-review.component';
import { TableUserComponent } from './components/table-info/table-user.component';
import { DocumentsViewsComponent } from './components/documents-views/documents-views.component';
import { SharedModule } from "src/app/shared/shared.module";
import { ChangePassComponent } from "./pages/change-pass/change-pass.component";
import { CohorteComponent } from "./pages/cohorte/cohorte.component";
import { PersonalComponent } from "./pages/personal/personal.component";
import { NotificationsComponent } from "../../shared/components/notifications/notifications.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PruebaComponent } from "./pages/prueba/prueba.component";
import { EntrevistaComponent } from "./pages/entrevista/entrevista.component";

@NgModule({
  declarations: [
    StepperComponent,
    PersonalInfoComponent,
    DocumentsComponent,
    StatusComponent,
    TableComponent,
    DocsReviewComponent,
    DocumentsViewsComponent,
    TableUserComponent,
    ChangePassComponent,
    CohorteComponent,
    PersonalComponent,
    EntrevistaComponent,
    PruebaComponent,
  ],
  imports: [
    CommonModule,
    InscriptionRoutingModule,
    AngularMaterialModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  providers: [DatePipe],
})
export class InscriptionModule { }
