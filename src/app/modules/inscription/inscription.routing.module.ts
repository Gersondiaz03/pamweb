import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StepperComponent } from './components/stepper/stepper.component';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { StatusComponent } from './components/status/status.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { DashboardComponent } from 'src/app/layout/dashboard/dashboard.component';
import { TableComponent } from './pages/home/table.component';
import { DocsReviewComponent } from './pages/docs-review/docs-review.component';
import { ChangePassComponent } from './pages/change-pass/change-pass.component';
import { CohorteComponent } from './pages/cohorte/cohorte.component';
import { PersonalComponent } from './pages/personal/personal.component';
import { NotificationsComponent } from 'src/app/shared/components/notifications/notifications.component';
import { EntrevistaComponent } from './pages/entrevista/entrevista.component';
import { PruebaComponent } from './pages/prueba/prueba.component';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
import { RoleGuard } from 'src/app/shared/guards/role.guard';

const routes: Routes = [
  {
    path: 'aspirante',
    component: StepperComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: PersonalInfoComponent,
      },
      { path: 'personal-info', component: PersonalInfoComponent },
      { path: 'documents', component: DocumentsComponent },
      { path: 'status', component: StatusComponent },
      { path: 'notifications', component: NotificationsComponent},
    ],
  },
  {
    path: 'cambiar-contraseña',
    component: ChangePassComponent,
  },
  { path: 'notifications', component: NotificationsComponent},
  
  {
    path: 'backlog',
    component: DashboardComponent,
    canActivateChild: [RoleGuard],
    children: [
      {
        path: 'admin',
        canActivateChild: [AuthGuard],
        children: [
          { path: 'cohortes', component: CohorteComponent },
          { path: 'personal', component: PersonalComponent},
          { path: 'entrevista', component: EntrevistaComponent},
          { path: 'prueba', component: PruebaComponent},
        ]
      },
      { path: 'home', component: TableComponent },
      { path: 'docs-review/:id', component: DocsReviewComponent},
      { path: '**', redirectTo: 'home' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptionRoutingModule {}
