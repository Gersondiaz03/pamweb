import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';

@Component({
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.css'],
})
export class ChangePassComponent  {
/**
 * TODO: falta implementar cambiar contraseña
 */

  public confirmForm: FormGroup = this.fb.group(
    {
      // Validaciones de obligatorio a los inputs
      currentPass: ['', [Validators.required]],
      newPass: ['', [Validators.required]],
      repeatPass: ['', [Validators.required]],
    },
    // Validador de que ambos inputs deben coincidir
    { validator: this.matchingFieldsValidator('newPass', 'repeatPass') }
  );

  constructor(private fb: FormBuilder) {}
// Validadores de inputs
  isValidField(field: string): boolean | null {
    return (
      this.confirmForm.controls[field].errors &&
      this.confirmForm.controls[field].touched
    );
  }
// Validar que las contraseñas coincidan
  matchingFieldsValidator(field1: string, field2: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value1 = control.get(field1)?.value;
      const value2 = control.get(field2)?.value;

      if (value1 !== value2) {
        return { matchingFields: true };
      }

      return null;
    };
  }
// Invoca las validaciones
  onSave(): void {
    if (this.confirmForm.invalid) {
      this.confirmForm.markAllAsTouched();
      return;
    }
  }


}
