import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Cohorte } from 'src/app/data/interfaces/cohorte.interface';
import { CohorteService } from '../../services/cohorte.service';

import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';



@Component({
  templateUrl: './cohorte.component.html',
  styleUrls: ['./cohorte.component.css']
})
export class CohorteComponent implements OnInit{

  //Lista de cohortes 
  public listCohortes: Cohorte[] = []
  // variable de control para el cohorte actual.
  public currentCohorte!: Cohorte | null;

  constructor(
    private _fb: FormBuilder,
    private cohorteService: CohorteService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    //Cuando se renderice el componente, se hace el llamado a la api para pintar los cohortes en la pantalla 
    this.cohorteService.cohortesList().subscribe({
      next: cohortes => {
        this.listCohortes = cohortes;
      }
    })
    this.openCohorte();
  }

  /**
   * Método para obtener la información del cohorte actual abierto si hay
   */
  public openCohorte(){
    this.cohorteService.currentCohorte.subscribe({
      next: cohorte => {
        this.currentCohorte = cohorte
      }
    })
  }

  /**
   * Método para cerrar un cohorte. 
   */
  public closeCohorte(){
    Swal.fire({
      title: '¿Estás seguro de que desea cerrar el cohorte actual?',
      text: 'Esta acción no se puede deshacer',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, cerrar',
    }).then(response => {
        if(response.isDenied || response.isDismissed)
          return 
        
        this.cohorteService.closeCurrentCohorte().subscribe({
          next: (res):any => {

            Swal.fire({
              title: `${res.message}`,
              icon: 'success',
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Aceptar',
            })
          }
        });
    })
  }

  /**
   * Método para abrir un nuevo cohorte. 
   */
  public openNewCohorte(){
    const starDate = new Date();
    this.spinner.show();
    this.cohorteService.openNewCohorte({fecha_inicio:starDate, fecha_fin: starDate}).subscribe({
      next: res => {
        this.spinner.hide();
        window.location.reload();
      },
      error: err => {
        this.spinner.hide();
        console.log(err.message)
      }
    })
  }
}
