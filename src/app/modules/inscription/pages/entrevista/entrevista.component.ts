import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/data/interfaces/user.example.interface';
import Swal from 'sweetalert2';
import { UserService } from '../../services/user.service';
import { CohorteService } from '../../services/cohorte.service';

@Component({
  selector: 'app-entrevista',
  templateUrl: './entrevista.component.html',
  styleUrls: ['./entrevista.component.css']
})
export class EntrevistaComponent implements OnInit{

  public minDate: Date;
  public users: User[] = [];
  public isOpenCohorte = false;

  constructor(
    private _userService: UserService,
    private cohorteS: CohorteService
  ){
    // Fecha mínima a mostrar desde hoy
    this.minDate = new Date();
  }

  ngOnInit(): void {
    this.cohorteS.currentCohorte.subscribe({
      next: res => {
        if(res != null)
          this.isOpenCohorte = true
      }
    });

    this.getUsers();
  }

  /**
   * Método para listar a todos los usuarios ASPIRANTE listos para entrevista y prueba
   *
   * @params
   * @return Lista con todos los usuarios ASPIRANTE listos para entrevista y prueba
   */
  getUsers(): void {
    this._userService.listUsersfilter().subscribe((users) => {
      this.users = [...users];
    });
  }

  //Mensaje tipo modal de confirmacion envio enlace
  public sendLink() {
    Swal.fire({
      title: 'Enlace enviado correctamente',
      icon: 'success',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Aceptar'
    }).then(result => {
      return
    })

  }
}
