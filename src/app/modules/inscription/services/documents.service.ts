import { Injectable } from '@angular/core';
import { JwtService } from '../../auth/services/jwt.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class DocumentsService {

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor(
    private http: HttpClient,
    private _router: Router,
    private readonly jwtService: JwtService
  ) {}


  public listUserDocuments(id: number): Observable<any> {
    const url = `${environment.apiBaseUrl}/doc/listar?userId=${id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this.jwtService.getToken(),
      }),
    };
    
    return this.http.get(url,httpOptions);
  }
}
