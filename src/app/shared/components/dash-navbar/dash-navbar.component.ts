/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @angular-eslint/no-output-native */
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';


@Component({
  selector: 'app-dash-navbar',
  templateUrl: './dash-navbar.component.html',
  styleUrls: ['./dash-navbar.component.css'],
})
export class DashNavbarComponent implements OnInit {

  @Output() close = new EventEmitter<boolean>();
  private flagClose = false;

  private _showNotifications = false;


  public isNewUser = true;

  private _roleUser = ''

  public user:any = {}

  constructor(
    private _authService: AuthService
  ) {}

  public get showNotifications(){
    return this._showNotifications
  }


  ngOnInit(): void {
    this._authService.currentUser.subscribe({
      next: user => {
        this.user = user ?? {}
      }
    })
  }

  public get notifications() {
    return this._showNotifications;
  }

  closeNav() {
    this.flagClose = !this.flagClose;
    this.close.emit(this.flagClose);
  }

  logout() {
    this._authService.logout();
  }

  public showNotificaciones() {
    return (this._showNotifications = !this._showNotifications);
  }

  public validateRole(){
    // obtengo las autorizaciones
    this._authService.currentUser.subscribe({
      next: ({role}) => this._roleUser = role
    })
    //para saber si el rol es usuario y no mostrar informacion.
    // this.isNewUser = this.r == 'ROLE_USUARIO'


  }
}
