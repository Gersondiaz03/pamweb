import { Component } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent {
/**
 * TODO: falta impplementar notificaciones.
*/

  listNotific = [
    {
      message: 'Señor John Doe, el documento "Hoja de vida" presenta inconsistencias. Por favor, enviarlo de nuevo. Gracias.',
      status: true,
      date: '12/04/2023',
    },
    {
      message: 'Señor John Doe, los documentos enviados durante la fase de inscripción fueron aprobados éxitosamente.',
      status: true,
      date: '24/04/2023',
    },
    {
      message: 'Señor John Doe, se le ha asignado la fecha y hora para la realización de la entrevista.',
      status: false,
      date: '05/05/2023',
    },
  ]

  public getMensajeLeido(mensajeLeido: boolean): string {
    return mensajeLeido ? 'Leído' : 'No leído';
  }

  cantidadLeida = this.listNotific.reduce((contador, notificacion) => {
    if (notificacion.status) {
      contador++;
    }
    return contador;
  }, 0);
}
