import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeNGModule } from '../modules/prime-ng/prime-ng.module';
import { DashNavbarComponent } from './components/dash-navbar/dash-navbar.component';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    DashNavbarComponent,
    SidebarComponent,
    NotificationsComponent,
    NotFoundComponent
  ],
  exports: [
    DashNavbarComponent,
    SidebarComponent,
    PrimeNGModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PrimeNGModule,
    NgxSpinnerModule,
  ],
})
export class SharedModule {}
