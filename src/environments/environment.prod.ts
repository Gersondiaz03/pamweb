export const environment = {
  production: true,
  apiBaseUrl: 'https://pam-api.herokuapp.com/',
};
